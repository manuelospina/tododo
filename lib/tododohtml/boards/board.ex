defmodule Tododohtml.Boards.Board do
  use Ecto.Schema
  import Ecto.Changeset

  schema "boards" do
    belongs_to(:user, Tododohtml.Accounts.User)
    has_many(:tasks, Tododohtml.Boards.Task)
    field(:title, :string)

    timestamps()
  end

  @doc false
  def changeset(board, attrs) do
    board
    |> cast(attrs, [:user_id, :title])
    |> validate_required([:title])
  end
end
