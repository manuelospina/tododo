defmodule Tododohtml.Boards.Task do
  use Ecto.Schema
  import Ecto.Changeset

  schema "tasks" do
    belongs_to(:board, Tododohtml.Boards.Board)
    belongs_to(:parent, Tododohtml.Boards.Task)
    has_many(:tasks, Tododohtml.Boards.Task, foreign_key: :parent_id)
    field(:description, :string)
    field(:status, :string)

    timestamps()
  end

  @doc false
  def changeset(task, attrs) do
    task
    |> cast(attrs, [:board_id, :description, :status])
    |> validate_required([:description, :status])
  end
end
