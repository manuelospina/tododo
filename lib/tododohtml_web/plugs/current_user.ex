defmodule TododohtmlWeb.Plugs.CurrentUser do
  import Plug.Conn
  alias Tododohtml.Accounts

  @session_name :user_id
  @assign_name :current_user

  def init(opts), do: opts

  def call(conn, _opts) do
    user_id = get_session(conn, @session_name)
    user = user_id && Accounts.get_user(user_id)
    assign_user(conn, user)
  end

  def assign_user(conn, user) do
    conn
    |> assign(@assign_name, user)
  end

  def set(conn, user) do
    conn
    |> assign_user(user)
    |> put_session(@session_name, user.id)
    |> configure_session(renew: true)
  end

  def get(conn), do: conn.assigns[@assign_name]

  def forget(conn) do
    conn
    |> delete_session(@session_name)
    |> configure_session(drop: true)
  end
end
