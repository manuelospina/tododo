defmodule TododohtmlWeb.SubtaskController do
  use TododohtmlWeb, :controller

  alias Tododohtml.Boards
  alias Tododohtml.Boards.Task

  def new(conn, %{"task_id" => id}) do
    task = Boards.get_task!(id)
    changeset = Boards.change_task(%Task{})
    render(conn, "new.html", task: task, changeset: changeset)
  end

  def create(conn, %{"task_id" => id, "description" => description}) do
    create(conn, %{"task_id" => id, "task" => %{"description" => description}})
  end

  def create(conn, %{"task_id" => id, "task" => subtask_params}) do
    task = Boards.get_task!(id)

    attrs =
      subtask_params
      |> Map.put_new("status", "to do")

    case Boards.add_task(task, attrs) do
      {:ok, task} ->
        conn
        |> put_flash(:info, "Task updated successfully.")
        |> redirect(to: board_path(conn, :show, task.board_id))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", task: task, changeset: changeset)
    end
  end
end
