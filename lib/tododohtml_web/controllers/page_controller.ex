defmodule TododohtmlWeb.PageController do
  use TododohtmlWeb, :controller

  alias Tododohtml.Accounts
  alias Tododohtml.Accounts.User
  alias Tododohtml.Boards

  alias TododohtmlWeb.Plugs.CurrentUser

  def index(conn, _params) do
    if user = CurrentUser.get(conn) do
      board = Boards.get_board!(user, "default")
      redirect(conn, to: board_path(conn, :show, board))
    else
      changeset = Accounts.change_user(%User{})
      render(conn, "index.html", changeset: changeset)
    end
  end
end
