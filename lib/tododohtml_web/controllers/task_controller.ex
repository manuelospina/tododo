defmodule TododohtmlWeb.TaskController do
  use TododohtmlWeb, :controller

  alias Tododohtml.Boards
  alias Tododohtml.Boards.Task
  alias TododohtmlWeb.Plugs.CurrentUser

  def index(conn, _params) do
    tasks = Boards.list_tasks()
    render(conn, "index.html", tasks: tasks)
  end

  def new(conn, _params) do
    changeset = Boards.change_task(%Task{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"description" => description}) do
    create(conn, %{"task" => %{"description" => description}})
  end

  def create(conn, %{"task" => task_params}) do
    user = CurrentUser.get(conn)
    board = Boards.get_board!(user, "default")

    attr =
      task_params
      |> Map.put_new("board_id", board.id)
      |> Map.put_new("status", "to do")

    case Boards.create_task(attr) do
      {:ok, task} ->
        conn
        |> put_flash(:info, "Task created successfully.")
        |> redirect(to: board_path(conn, :show, task.board_id))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    task = Boards.get_task!(id)
    render(conn, "show.html", task: task)
  end

  def edit(conn, %{"id" => id}) do
    task = Boards.get_task!(id)
    changeset = Boards.change_task(task)
    render(conn, "edit.html", task: task, changeset: changeset)
  end

  def update(conn, %{"id" => id, "task" => task_params}) do
    task = Boards.get_task!(id)

    case Boards.update_task(task, task_params) do
      {:ok, task} ->
        conn
        |> put_flash(:info, "Task updated successfully.")
        |> redirect(to: task_path(conn, :show, task))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", task: task, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    task = Boards.get_task!(id)
    {:ok, _task} = Boards.delete_task(task)

    conn
    |> put_flash(:info, "Task deleted successfully.")
    |> redirect(to: task_path(conn, :index))
  end
end
