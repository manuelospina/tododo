defmodule TododohtmlWeb.SessionController do
  use TododohtmlWeb, :controller
  alias Tododohtml.Accounts
  alias Tododohtml.Boards
  alias TododohtmlWeb.Plugs.CurrentUser

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, %{"credentials" => %{"email" => email, "password" => password}}) do
    case Accounts.authenticate_user(email, password) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Welcome to Tododo")
        |> CurrentUser.set(user)
        |> redirect_to_board(user, "default")

      {:error, _} ->
        conn
        |> put_flash(:error, "Unable to sign in")
        |> render("new.html")
    end
  end

  defp redirect_to_board(conn, user, title) do
    board = Boards.get_board!(user, title)
    redirect(conn, to: board_path(conn, :show, board))
  end

  def delete(conn, _params) do
    CurrentUser.forget(conn)
    |> redirect(to: page_path(conn, :index))
  end
end
