defmodule TododohtmlWeb.Router do
  use TododohtmlWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(TododohtmlWeb.Plugs.CurrentUser)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", TododohtmlWeb do
    # Use the default browser stack
    pipe_through(:browser)

    get("/", PageController, :index)
    resources("/users", UserController)
    resources("/sessions", SessionController, singleton: true, only: [:new, :create, :delete])
    resources("/boards", BoardController)

    resources "/tasks", TaskController do
      resources("/subtasks", SubtaskController, only: [:new, :create])
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", TododohtmlWeb do
  #   pipe_through :api
  # end
end
