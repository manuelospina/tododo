defmodule TododohtmlWeb.CurrentUserPlugTest do
  use TododohtmlWeb.ConnCase

  alias Tododohtml.Accounts

  @moduletag :accounts_test_suite

  @user_attrs %{
    email: "john.dow@test.ltd",
    password: "123456"
  }

  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(@user_attrs)
      |> Accounts.create_user()

    user
  end

  describe "call/2" do
    test "assign user to nil if there is not user" do
      conn =
        session_conn()
        |> TododohtmlWeb.Plugs.CurrentUser.call(%{})

      assert conn.assigns.current_user == nil
    end

    test "assign user to logged in user" do
      user = user_fixture()

      conn =
        session_conn()
        |> put_session(:user_id, user.id)
        |> TododohtmlWeb.Plugs.CurrentUser.call(%{})

      assert conn.assigns.current_user.id == user.id
      assert conn.assigns.current_user.email == user.email
    end
  end

  describe "set/2" do
    test "put session, assign user and is_admin" do
      user = user_fixture()

      conn =
        session_conn()
        |> TododohtmlWeb.Plugs.CurrentUser.set(user)

      assert conn.assigns.current_user.id == user.id
      assert conn.assigns.current_user.email == user.email
      assert get_session(conn, :user_id) == user.id
    end
  end

  describe "get/1" do
    test "returns assigned current user" do
      user = user_fixture()

      conn =
        session_conn()
        |> put_session(:user_id, user.id)
        |> TododohtmlWeb.Plugs.CurrentUser.call(%{})

      returned_user = TododohtmlWeb.Plugs.CurrentUser.get(conn)
      assert returned_user.id == user.id
      assert returned_user.email == user.email
    end
  end

  describe "forget/1" do
    test "Delete assigned current user" do
      user = user_fixture()

      conn =
        session_conn()
        |> put_session(:user_id, user.id)
        |> TododohtmlWeb.Plugs.CurrentUser.call(%{})

      assert get_session(conn, :user_id)

      conn = TododohtmlWeb.Plugs.CurrentUser.forget(conn)
      refute get_session(conn, :user_id)
    end
  end
end
