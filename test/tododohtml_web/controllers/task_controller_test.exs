defmodule TododohtmlWeb.TaskControllerTest do
  use TododohtmlWeb.ConnCase

  alias Tododohtml.Boards
  alias Tododohtml.Accounts

  @create_attrs %{description: "some description", status: "some status"}
  @update_attrs %{description: "some updated description", status: "some updated status"}
  @invalid_attrs %{description: nil, status: nil}

  def fixture(:task) do
    {:ok, task} = Boards.create_task(@create_attrs)
    task
  end

  def fixture(:board) do
    {:ok, board} = Boards.create_board(%{title: "default"})
    board
  end

  describe "index" do
    test "lists all tasks", %{conn: conn} do
      conn = get(conn, task_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Tasks"
    end
  end

  describe "new task" do
    test "renders form", %{conn: conn} do
      conn = get(conn, task_path(conn, :new))
      assert html_response(conn, 200) =~ "New Task"
    end
  end

  describe "create task" do
    setup [:create_session_conn]

    test "redirects to board when data is valid", %{session_conn: conn, board: board} do
      conn =
        post(
          conn,
          task_path(conn, :create),
          task: %{
            "description" => "Some description",
          }
        )

      assert redirected_to(conn) == board_path(conn, :show, board.id)

      # conn = get conn, task_path(conn, :show, id)
      # assert html_response(conn, 200) =~ "Show Task"
    end

    test "redirects to board when passing only the description", %{session_conn: conn, board: board} do
      conn =
        post(
          conn,
          task_path(conn, :create),
          %{
            "description" => "Some description",
          }
        )

      assert redirected_to(conn) == board_path(conn, :show, board.id)

      # conn = get conn, task_path(conn, :show, id)
      # assert html_response(conn, 200) =~ "Show Task"
    end

    test "renders errors when data is invalid", %{session_conn: conn} do
      conn = post(conn, task_path(conn, :create), task: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Task"
    end

    test "renders errors when description is invalid", %{session_conn: conn} do
      conn = post(conn, task_path(conn, :create), %{"description" => nil})
      assert html_response(conn, 200) =~ "New Task"
    end
  end

  describe "edit task" do
    setup [:create_task]

    test "renders form for editing chosen task", %{conn: conn, task: task} do
      conn = get(conn, task_path(conn, :edit, task))
      assert html_response(conn, 200) =~ "Edit Task"
    end
  end

  describe "update task" do
    setup [:create_task]

    test "redirects when data is valid", %{conn: conn, task: task} do
      conn = put(conn, task_path(conn, :update, task), task: @update_attrs)
      assert redirected_to(conn) == task_path(conn, :show, task)

      conn = get(conn, task_path(conn, :show, task))
      assert html_response(conn, 200) =~ "some updated description"
    end

    test "renders errors when data is invalid", %{conn: conn, task: task} do
      conn = put(conn, task_path(conn, :update, task), task: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Task"
    end
  end

  describe "delete task" do
    setup [:create_task]

    test "deletes chosen task", %{conn: conn, task: task} do
      conn = delete(conn, task_path(conn, :delete, task))
      assert redirected_to(conn) == task_path(conn, :index)

      assert_error_sent(404, fn ->
        get(conn, task_path(conn, :show, task))
      end)
    end
  end

  defp create_task(_) do
    task = fixture(:task)
    {:ok, task: task}
  end

  defp create_session_conn(_) do
    {:ok, user} =
      Accounts.create_user(%{
        email: "john.dow@test.ltd",
        password: "123456"
      })

    {:ok, board} = Boards.create_board(%{user_id: user.id, title: "default"})

    session_conn =
      session_conn()
      |> TododohtmlWeb.Plugs.CurrentUser.set(user)

    {:ok, session_conn: session_conn, board: board}
  end
end
