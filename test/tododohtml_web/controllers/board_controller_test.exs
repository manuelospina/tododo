defmodule TododohtmlWeb.BoardControllerTest do
  use TododohtmlWeb.ConnCase

  alias Tododohtml.Boards
  alias Tododohtml.Accounts

  @create_attrs %{title: "some title"}
  @update_attrs %{title: "some updated title"}
  @invalid_attrs %{title: nil}

  def fixture(:board) do
    {:ok, board} = Boards.create_board(@create_attrs)
    board
  end

  describe "index" do
    setup [:create_session_conn]

    test "lists all boards", %{session_conn: conn} do
      conn = get(conn, board_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Boards"
    end
  end

  describe "new board" do
    setup [:create_session_conn]
    test "renders form", %{session_conn: conn} do
      conn = get(conn, board_path(conn, :new))
      assert html_response(conn, 200) =~ "New Board"
    end
  end

  describe "create board" do
    setup [:create_session_conn]
    test "redirects to show when data is valid", %{session_conn: conn, user: user} do
      valid_attrs = Map.put(@create_attrs, :user_id, user.id)
      conn = post(conn, board_path(conn, :create), board: valid_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == board_path(conn, :show, id)

      conn = get(conn, board_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Tododo Board"
    end

    test "renders errors when data is invalid", %{session_conn: conn} do
      conn = post(conn, board_path(conn, :create), board: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Board"
    end
  end

  describe "show board" do
    setup [:create_session_conn]

    test "show board", %{session_conn: conn, board: board} do
      conn = get(conn, board_path(conn, :show, board))
      assert html_response(conn, 200) =~ "Tododo Board"
    end

    @tag :skip
    test "board can only be see by its owner", %{session_conn: conn} do
      board = fixture(:board)
      assert %{status: 403} = get(conn, board_path(conn, :show, board))
    end
  end

  describe "edit board" do
    setup [:create_session_conn]

    test "renders form for editing chosen board", %{session_conn: conn, board: board} do
      conn = get(conn, board_path(conn, :edit, board))
      assert html_response(conn, 200) =~ "Edit Board"
    end
  end

  describe "update board" do
    setup [:create_session_conn]

    test "redirects when data is valid", %{session_conn: conn, board: board} do
      conn = put(conn, board_path(conn, :update, board), board: @update_attrs)
      assert redirected_to(conn) == board_path(conn, :show, board)
      conn = get(conn, board_path(conn, :show, board))
      assert html_response(conn, 200) =~ "Tododo Board"
    end

    test "renders errors when data is invalid", %{session_conn: conn, board: board} do
      conn = put(conn, board_path(conn, :update, board), board: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Board"
    end
  end

  describe "delete board" do
    setup [:create_session_conn]

    test "deletes chosen board", %{session_conn: conn, board: board} do
      conn = delete(conn, board_path(conn, :delete, board))
      assert redirected_to(conn) == board_path(conn, :index)

      assert_error_sent(404, fn ->
        get(conn, board_path(conn, :show, board))
      end)
    end
  end

  defp create_session_conn(_) do
    {:ok, user} =
      Accounts.create_user(%{
        email: "john.dow@test.ltd",
        password: "123456"
      })

    {:ok, board} = Boards.create_board(%{user_id: user.id, title: "default"})

    session_conn =
      session_conn()
      |> TododohtmlWeb.Plugs.CurrentUser.set(user)

    {:ok, session_conn: session_conn, board: board, user: user}
  end
end
