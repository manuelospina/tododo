defmodule TododohtmlWeb.SessionControllerTest do
  use TododohtmlWeb.ConnCase

  alias Tododohtml.Accounts
  alias Tododohtml.Boards

  @moduletag :accounts_test_suite

  @user_attrs %{
    email: "john.dow@test.ltd",
    password: "123456"
  }

  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(@user_attrs)
      |> Accounts.create_user()

    user
  end

  def board_fixture(user) do
    {:ok, board} = Boards.create_board(%{user_id: user.id, title: "default"})

    board
  end

  describe "GET /sessions/new" do
    test "Return 200 status", %{conn: conn} do
      assert %{status: 200} = get(conn, session_path(conn, :new))
    end
  end

  describe "POST /sessions (:create)" do
    test "Return 302(redirect) status if attributes are valid", %{conn: conn} do
      user = user_fixture()
      board_fixture(user)

      assert %{status: 302} =
               post(conn, session_path(conn, :create), %{
                 "credentials" => %{
                   "email" => @user_attrs.email,
                   "password" => @user_attrs.password
                 }
               })
    end

    test "redirect to boards when passing valid attributes", %{conn: conn} do
      user = user_fixture()
      board = board_fixture(user)

      conn =
        post(conn, session_path(conn, :create), %{
          "credentials" => %{"email" => @user_attrs.email, "password" => @user_attrs.password}
        })

      assert redirected_to(conn, 302) == board_path(conn, :show, board)
    end

    test "render new when passing invalid attributes", %{conn: conn} do
      %{status: 200, request_path: "/sessions"} =
        post(conn, session_path(conn, :create), %{
          "credentials" => %{"email" => @user_attrs.email, "password" => nil}
        })
    end
  end

  describe "DELETE /sessions/ delete" do
    test "Return 302 status on success", %{conn: conn} do
      user = user_fixture()

      assert %{status: 302} =
               user_session_conn(user)
               |> delete(session_path(conn, :delete))
    end

    test "redirect to index", %{conn: conn} do
      user = user_fixture()

      conn =
        user_session_conn(user)
        |> delete(session_path(conn, :delete))

      assert redirected_to(conn, 302) == "/"
    end

    test "redirect even when there is not session to delete", %{conn: conn} do
      conn = delete(conn, session_path(conn, :delete))
      assert redirected_to(conn, 302) == "/"
    end
  end
end
