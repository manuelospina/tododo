defmodule TododohtmlWeb.PageControllerTest do
  use TododohtmlWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "tododo is a task manager"
  end
end
