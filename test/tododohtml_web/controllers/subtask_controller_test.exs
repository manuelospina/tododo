defmodule TododohtmlWeb.SubtaskControllerTest do
  use TododohtmlWeb.ConnCase

  alias Tododohtml.Boards
  alias Tododohtml.Accounts

  @create_attrs %{description: "some description", status: "to do"}
  @invalid_attrs %{description: nil}

  def fixture(:task) do
    {:ok, task} = Boards.create_task(@create_attrs)
    task
  end

  def fixture(:board) do
    {:ok, board} = Boards.create_board(%{title: "default"})
    board
  end

  describe "new subtask" do
    setup [:create_task]

    test "renders form", %{conn: conn, task: task} do
      conn = get(conn, task_subtask_path(conn, :new, task))
      assert html_response(conn, 200) =~ "New Subtask"
    end
  end

  describe "create subtask" do
    setup [:create_session]

    test "redirects to board when passing only the description", %{session_conn: conn, board: board, task: task} do
      conn =
        post(
          conn,
          task_subtask_path(conn, :create, task),
          %{
            "description" => "Some description",
          }
        )

      assert redirected_to(conn) == board_path(conn, :show, board.id)
    end


    test "redirects to board when data is valid", %{session_conn: conn, board: board, task: task} do
      conn =
        post(
          conn,
          task_subtask_path(conn, :create, task),
          task: %{
            "description" => "Some description",
          }
        )

      assert redirected_to(conn) == board_path(conn, :show, board.id)
    end

    test "renders errors when data is invalid", %{session_conn: conn, task: task} do
      conn = post(conn, task_subtask_path(conn, :create, task), task: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Subtask"
    end
  end

  defp create_task(_) do
    task = fixture(:task)
    {:ok, task: task}
  end

  defp create_session(_) do
    {:ok, user} =
      Accounts.create_user(%{
        email: "john.dow@test.ltd",
        password: "123456"
      })

    {:ok, board} = Boards.create_board(%{user_id: user.id, title: "default"})

    {:ok, task} =
      Boards.create_task(%{board_id: board.id, description: "some description", status: "to do"})

    session_conn =
      session_conn()
      |> TododohtmlWeb.Plugs.CurrentUser.set(user)

    {:ok, session_conn: session_conn, board: board, task: task}
  end
end
