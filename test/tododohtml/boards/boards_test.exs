defmodule Tododohtml.BoardsTest do
  use Tododohtml.DataCase

  alias Tododohtml.Accounts
  alias Tododohtml.Boards

  @moduletag :boards_test_suite

  @user_attrs %{
    email: "john.dow@test.ltd",
    password: "123456"
  }

  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(@user_attrs)
      |> Accounts.create_user()

    user
  end

  describe "boards" do
    alias Tododohtml.Boards.Board

    @valid_attrs %{title: "some title"}
    @update_attrs %{title: "some updated title"}
    @invalid_attrs %{title: nil}

    def board_fixture(attrs \\ %{}) do
      {:ok, board} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Boards.create_board()

      board
    end

    test "list_boards/0 returns all boards" do
      board = board_fixture()
      assert Boards.list_boards() == [board]
    end

    test "get_board!/1 returns the board with given id" do
      board = board_fixture()
      assert %Board{} = result = Boards.get_board!(board.id)
      assert result.id == board.id
      assert result.title == @valid_attrs.title
    end

    test "get_board!/2 returns the board with given user and title" do
      user = user_fixture()
      attr = Enum.into(@valid_attrs, %{user_id: user.id})
      board = board_fixture(attr)

      {:ok, task} =
        Boards.create_task(%{description: "a description", status: "to do", board_id: board.id})

      assert %Board{} = result = Boards.get_board!(user, "some title")
      assert result.id == board.id
      assert result.user_id == user.id
      assert [%Boards.Task{} = first_task | _tail] = result.tasks
      assert task.id == first_task.id
    end

    test "get_user_board!/2 returns the board with given user and id" do
      user = user_fixture()
      attr = Enum.into(@valid_attrs, %{user_id: user.id})
      board = board_fixture(attr)

      {:ok, task} =
        Boards.create_task(%{description: "a description", status: "to do", board_id: board.id})

      assert %Board{} = result = Boards.get_user_board!(user, board.id)
      assert result.id == board.id
      assert result.user_id == user.id
      assert [%Boards.Task{} = first_task | _tail] = result.tasks
      assert task.id == first_task.id
    end

    test "create_board/1 with valid data creates a board" do
      user = user_fixture()
      attr = Enum.into(@valid_attrs, %{user_id: user.id})
      assert {:ok, %Board{} = board} = Boards.create_board(attr)
      assert board.title == "some title"
      assert board.user_id == user.id
    end

    test "create_board/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Boards.create_board(@invalid_attrs)
    end

    test "update_board/2 with valid data updates the board" do
      board = board_fixture()
      assert {:ok, board} = Boards.update_board(board, @update_attrs)
      assert %Board{} = board
      assert board.title == "some updated title"
    end

    test "update_board/2 with invalid data returns error changeset" do
      board = board_fixture()
      assert {:error, %Ecto.Changeset{}} = Boards.update_board(board, @invalid_attrs)
      assert %Board{} = result = Boards.get_board!(board.id)
      assert result.id == board.id
      assert result.title == @valid_attrs.title
    end

    test "delete_board/1 deletes the board" do
      board = board_fixture()
      assert {:ok, %Board{}} = Boards.delete_board(board)
      assert_raise Ecto.NoResultsError, fn -> Boards.get_board!(board.id) end
    end

    test "change_board/1 returns a board changeset" do
      board = board_fixture()
      assert %Ecto.Changeset{} = Boards.change_board(board)
    end
  end

  describe "tasks" do
    alias Tododohtml.Boards.Task

    @valid_attrs %{description: "some description", status: "some status"}
    @update_attrs %{description: "some updated description", status: "some updated status"}
    @invalid_attrs %{description: nil, status: nil}

    def task_fixture(attrs \\ %{}) do
      {:ok, task} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Boards.create_task()

      task
    end

    test "list_tasks/0 returns all tasks" do
      task = task_fixture()
      assert Boards.list_tasks() == [task]
    end

    test "get_task!/1 returns the task with given id" do
      task = task_fixture()
      assert Boards.get_task!(task.id) == task
    end

    test "create_task/1 with valid data creates a task" do
      assert {:ok, %Task{} = task} = Boards.create_task(@valid_attrs)
      assert task.description == "some description"
      assert task.status == "some status"
    end

    test "create_task/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Boards.create_task(@invalid_attrs)
    end

    test "update_task/2 with valid data updates the task" do
      task = task_fixture()
      assert {:ok, task} = Boards.update_task(task, @update_attrs)
      assert %Task{} = task
      assert task.description == "some updated description"
      assert task.status == "some updated status"
    end

    test "update_task/2 with invalid data returns error changeset" do
      task = task_fixture()
      assert {:error, %Ecto.Changeset{}} = Boards.update_task(task, @invalid_attrs)
      assert task == Boards.get_task!(task.id)
    end

    test "delete_task/1 deletes the task" do
      task = task_fixture()
      assert {:ok, %Task{}} = Boards.delete_task(task)
      assert_raise Ecto.NoResultsError, fn -> Boards.get_task!(task.id) end
    end

    test "change_task/1 returns a task changeset" do
      task = task_fixture()
      assert %Ecto.Changeset{} = Boards.change_task(task)
    end

    test "add tasks" do
      output = task_fixture()

      assert {:ok, task} =
               Boards.add_task(output, %{
                 description: "some updated description",
                 status: "to do"
               })

      assert %Task{} = task
      assert [%Task{}] = task.tasks

      child_task = hd(task.tasks)
      assert child_task.description == "some updated description"
      assert child_task.status == "to do"
    end
  end
end
