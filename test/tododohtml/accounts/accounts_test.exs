defmodule Tododohtml.AccountsTest do
  use Tododohtml.DataCase

  alias Tododohtml.Accounts

  @moduletag :accounts_test_suite

  describe "users" do
    alias Tododohtml.Accounts.User

    @valid_attrs %{email: "some email", password: "some password"}
    @update_attrs %{email: "some updated email", password: "some updated password"}
    @invalid_attrs %{email: nil, password: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Accounts.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.email == "some email"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = Accounts.update_user(user, @update_attrs)
      assert %User{} = user
      assert user.email == "some updated email"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
      assert user == Accounts.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end

    test "authenticate_user/2 returns a user when email and password match" do
      user_fixture()

      assert {:ok, %User{}} =
               Accounts.authenticate_user(@valid_attrs.email, @valid_attrs.password)
    end

    test "authenticate_user/2 returns an error when email is not found" do
      user_fixture()

      assert {:error, "invalid user-identifier"} =
               Accounts.authenticate_user("invalid@test.ltd", @valid_attrs.password)
    end

    test "authenticate_user/2 returns an error when password doesn't match" do
      user_fixture()

      assert {:error, "invalid password"} =
               Accounts.authenticate_user(@valid_attrs.email, "invalid")
    end
  end
end
