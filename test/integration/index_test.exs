defmodule Tododohtml.IndexTest do
  use ExUnit.Case
  use Hound.Helpers

  @moduletag :integration_tests

  @tododo_host "http://172.17.0.1:4001/"

  hound_session()

  @tag :skip
  test "Show index page" do
    navigate_to(@tododo_host)
    assert page_title() == "Tododo"
  end

  @tag :skip
  test "Index page contains sign up form" do
    navigate_to(@tododo_host)
    email_element = find_element(:name, "email")
    pwd_element = find_element(:name, "password")
    assert element_displayed?(email_element)
    assert element_displayed?(pwd_element)
  end
end
