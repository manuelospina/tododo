defmodule Tododohtml.DefaultBoardTest do
  use ExUnit.Case
  use Hound.Helpers

  @moduletag :integration_tests

  @tododo_host "http://172.17.0.1:4001/"

  hound_session()

  @tag :skip
  test "A user reached the default board after sign up" do
    navigate_to(@tododo_host <> "users/new")
    IO.inspect(current_url())
    assert page_title() == "Tododo"

    email_element = find_element(:id, "user_email")
    input_into_field(email_element, "user@example.com")
    pwd_element = find_element(:id, "user_password")
    fill_field(pwd_element, "123456")

    submit_element = find_element(:name, "submit")
    click(submit_element)

    assert current_path() == "/boards/1"
  end
end
