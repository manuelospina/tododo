function show_form(form_name) {
  var x = document.getElementById(form_name);
  if(x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

export { show_form }
