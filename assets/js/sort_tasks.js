// Sort tasks
function put_tasks_on_pannel(pannel, tasks){
  for (var i = 0 ; i < tasks.length ; i++) {
    pannel.appendChild(tasks[i]);
  }
}

function sort_tasks(status) {
  var tasks = document.querySelectorAll('.task.' + status);
  var pannel = document.querySelector('#' + status + '.task-list');
  put_tasks_on_pannel(pannel, tasks);
}

export { sort_tasks }

