// Drag and drop
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev, target) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    target.appendChild(document.getElementById(data));
    status_change(target, document.getElementById(data));
}

// Status change
function get_status(element) {
    return element.getAttribute("id");
}

function get_task_id(element) {
    return element.getAttribute("id");
}

function status_change(pannel, task) {
    var status = get_status(pannel);
    var task_id = get_task_id(task)
    change_status(task_id, status);
    // task.innerHTML = response;
}
  
// AJAX
function change_status(task_id, status) {
    var CSRF_TOKEN = window.csrf_token;
    var xhttp = new XMLHttpRequest();
    var data = new FormData();
    data.append("task[status]", status);
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            return this.responseText;
       }
    };
    xhttp.open("PATCH", "/tasks/" + task_id, true);
    xhttp.setRequestHeader("X-CSRF-Token", CSRF_TOKEN)
    xhttp.send(data);
}

export { drag, drop, allowDrop }
// sort_tasks("doing");
// sort_tasks("done");
