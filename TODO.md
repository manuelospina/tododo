# TODO

## to do

* When trying to access another user's board, it shows "Internal server error". Better to redirect to login
* Add life goal
* [PM] do initating a project process
  - Plan using lean canvas, arrra
  - create tasks for marketing content strategy
* Improve API for board (See programming phoenix Multimedia)
  * Set user_id in boards as required [see][1]
  * Board create always need to received a user_id. Shouldn't received a user to? think of where or =
  * **DONE** unsigned users are not allowed anywhere but index and login/signup
  * Make boards singleton in routes
  * Board tasks should be called through AJAX
* Improve tasks
  * It can be only three tasks on doing board
  * Item.status defaults to To Do in database?
  * Task.status can be [to do, doing, done]
    * Validate: validate_subset(changeset, :status, ["to do", "doing", "done"]) [see][3]
  * Check task_update, redirect and render are not needed
* Improve authentication:
  * password recovery
  * Email verification
  * Change accounts: separate users from authentication (See "Programming Phoenix")
* Move board and task to a new end point and require auth
* Edit and delete outcomes and tasks 
* Improve testing
  * Create integration test (python?) Hound is not working!
  * Add test for js
* Create CI
* Implement Single Page App [see][6], [see][7]
* Check style of email to match log out
* Connect through Patreons! [see][9]
* deploy on [beanstalk][14] Use docker save to create a tar [see][16]
* deploy on Google
* Deploy in webfaction
* Add timer for tasks
* Admin

## doing

* [PM] do starting up a project process
  - include lean canvas, arrra

## done

* **DONE** Rewrite JS as modules
  * Separate modules: drag_and_drop, show form, etc.
* **DONE** Some tasks are not changing to Done! This may happen when there is to many taks on the Done pannel [17]
* **DONE** Make sure a board is only seen by it's owner
* **DONE** drop session to avoid errors with cookies
* **DONE** if the user is not found, redirect to index
* **DONE** Board should require authentication
* **DONE** Fix Plug test
* **DONE** Fix controller board show. 
  * **DONE** Create get_default_user_board function in context)
  * **DONE** Fix page controller
  * **DONE** fix show in controller
* Clean-up code
  * **DONE** Clean up board_controller_test
  * **DONE** Change sign-in for log-in
* [MKT] Create content marketing plan
* Deploy in AWS [apache proxy][10], [elixir on aws][12], [aws tutorials][13], [docker and beanstalk][14]  
  * **DONE** Run dockers
  * **DONE** Proxy apache 80 to phoenix 4000
  * **DONE** Add domain name. For now, I'm using a elastic IPs
* Create docker and docker-compose [see][11]
  - **DONE** How to connect to the database from docker?
  - **DONE** install node
  - **DONE** ports? 
  - **DONE** Create docker-compose
* Secure password
* Session
* index
* Add Hound for integration testing:
  Hound is using selenium docker, so the IP address is the one given by: sudo docker inspect selenium-firefox | grep Gateway 
* Sign up form should be in the landing page.
* Create default board.
  * **DONE** Users should go to board after sign-in
  * **DONE** Board should have three columns: Todo, doing, done
  * **DONE** Login users should be redirected to board from index
  * **DONE** Users should be redirected to board after Login
  * **DONE** Fix board creation: change user_id on schema and migration 
* current user after sign up
* board_controller.show should call get_board!/2
* As a user I want to add a outcome in my board, so I can start organizing my tasks
  * **DONE** Add "new outcome" to the to do column in the board.
  * **DONE** new outcome should belong to board.
  * **DONE** New outcome should appear in the to do column after is added
  * **DONE** New outcome should default to "to do"
* Outcomes have tasks
  * **DONE** taks belongs_to tasks
  * **DONE** Add link to add tasks form
    * **DONE** Add subtask resources: [new, create] Need another route for it: tasks/parent_id/new
  * **DONE** Add tasks should appear under outcome [see][5]
* **DONE** Tasks can be move to other pannels - drag and drop (doing, done) [see][4]
  * CSRF_token for ajax+phoenix [see][8]
* Board should put tasks in the right pannel (to do, doing done)
* CSS: organize pannels
* Separate Javascript to it's own file
* New outcome form and new task should appear in board [see][2]
  * **DONE** Create outcome form hide/show
  * **DONE** Get interaction with backend
  * **DONE** Create subtask form
  * **DONE** Get interaction with backend
  * **DONE** The list of task should not be accesible. There is a link from add task to this page

[1]: https://medium.com/@lauraannwilliams/you-dont-have-to-use-put-assoc-or-cast-assoc-48b8575be3ce
[2]: https://www.w3schools.com/howto/howto_js_toggle_hide_show.asp
[3]: https://hexdocs.pm/ecto/Ecto.Changeset.html#validate_inclusion/4
[4]: https://www.html5rocks.com/en/tutorials/dnd/basics/
[5]: https://robots.thoughtbot.com/preloading-nested-associations-with-ecto
[6]: https://elixirforum.com/t/ajax-call-in-phoenix-respond-to-js-in-rails/3903/3
[7]: https://coligo.io/building-a-mobile-app-with-cordova-vuejs/
[8]: https://stackoverflow.solutions/question/show_question_details/12
[9]: https://docs.patreon.com/#introduction
[10]: https://github.com/grych/drab/wiki/Drab-in-production-and-behind-a-proxy
[11]: https://semaphoreci.com/community/tutorials/dockerizing-elixir-and-phoenix-applications
[12]: https://medium.com/mint-digital/elixir-deployments-on-aws-ee787aa02a9d
[13]: https://aws.amazon.com/getting-started/tutorials/
[14]: https://robots.thoughtbot.com/deploying-elixir-to-aws-elastic-beanstalk-with-docker
[15]: https://vnegrisolo.github.io/javascript/setup-twitter-bootstrap-on-phoenix-projects
[16]: https://blog.fazibear.me/deploy-elixir-application-74e1bce100c6
[17]:https://stackoverflow.com/questions/28203585/prevent-drop-inside-a-child-element-when-drag-dropping-with-js
