#!/bin/bash

while ! pg_isready -h postgres > /dev/null 2> /dev/null; do
  echo 'Waiting for PostgreSQL to be connectable...'
  sleep 3
done
echo "Connected to PostgreSQL!"

mix deps.get
mix ecto.create
mix ecto.migrate

mix phx.server
