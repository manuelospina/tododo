FROM elixir:1.6.5

ARG APP_PATH=/var/src/app/

WORKDIR $APP_PATH

RUN \
  set -x && \
  apt-get update && \
  apt-get install -y \
    postgresql-client \
    inotify-tools \
    --no-install-recommends && \
  rm -rf /var/lib/apt/lists/*

# Install elixir deps
COPY mix.* $APP_PATH
RUN MIX_ENV=prod mix local.hex --force
RUN MIX_ENV=prod mix local.rebar --force
RUN MIX_ENV=prod mix deps.get

# Install Node and  deps
COPY . $APP_PATH 
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - && apt-get install -y nodejs
RUN cd assets && npm install && cd ..

# Install the app
RUN MIX_ENV=prod mix compile

# Compile assets
RUN cd assets && node node_modules/brunch/bin/brunch build --production && cd ..
RUN MIX_ENV=prod mix phx.digest

EXPOSE 4000

CMD MIX_ENV=prod mix ecto.create && MIX_ENV=prod mix ecto.migrate && PORT=4000 MIX_ENV=prod mix phx.server
