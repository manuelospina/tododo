defmodule Tododohtml.Repo.Migrations.CreateTasks do
  use Ecto.Migration

  def change do
    create table(:tasks) do
      add :description, :string
      add :status, :string
      add :board_id, references(:boards, on_delete: :nothing)
      add :parent_id, references(:tasks, on_delete: :nothing)

      timestamps()
    end

    create index(:tasks, [:board_id])
    create index(:tasks, [:parent_id])
  end
end
